#!/usr/bin/env python

import MySQLdb


# delete the database tables
def drop_tables (curs):

    # drop tables
    curs.execute('DROP TABLE IF EXISTS enviro')
    curs.execute('DROP TABLE IF EXISTS alarm')
    curs.execute('DROP TABLE IF EXISTS movement')


# create database tables
def create_tables (curs):

    # create tables
    curs.execute("""CREATE TABLE enviro (
                                temp INT,
                                humid INT,
                                light INT,
                                sound INT,
                                updated DATETIME
                              )""")


    curs.execute("""CREATE TABLE alarm (
                                dist INT,
                                updated DATETIME
                              )""")


    curs.execute("""CREATE TABLE movement (
                                pir BOOLEAN,
                                updated DATETIME
                              )""")

# the program starts here
def main():

    print "Setting up database..."


    conn = MySQLdb.connect('127.0.0.1', 'rguser', 'Mj$AU$5t', 'rgsneuron')
    curs = conn.cursor()


    # delete the tables 
    try:
        drop_tables(curs)
        conn.commit()
        print "Dropped tables\n"
    except:
        conn.rollback()
        print "Failed to drop tables\n"


    # create the database tables
    try:
        create_tables(curs)
        conn.commit()
        print "Created tables\n"
    except:
        conn.rollback()
        print "Failed to create tables\n"


    # close the connection to the database
    conn.close()
    print "DB Connection closed"


if __name__ == "__main__":
    main()



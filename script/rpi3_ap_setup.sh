#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]
	then echo "Must be root"
	exit
fi

apt-get update
apt-get install dnsmasq hostapd

echo "denyinterfaces wlan0" >> /etc/dhcpcd.conf

sed -i -- 's/allow-hotplug wlan0//g' /etc/network/interfaces
sed -i -- 's/iface wlan0 inet manual//g' /etc/network/interfaces
sed -i -- 's/allow-hotplug wlan1//g' /etc/network/interfaces
sed -i -- 's/iface wlan1 inet manual//g' /etc/network/interfaces
sed -i -- 's/    wpa-conf \/etc\/wpa_supplicant\/wpa_supplicant.conf//g' /etc/network/interfaces

cat >> /etc/network/interfaces <<EOF
allow-hotplug wlan0
iface wlan0 inet static
	address 172.24.1.1
	netmask 255.255.255.0
	network 172.24.1.0
	broadcast 172.24.1.255

up iptables-restore < /etc/iptables.ipv4.nat

EOF

service dhcpcd restart
ifdown wlan0
ifup wlan0

cat > /etc/hostapd/hostapd.conf <<EOF
# This is the name of the WiFi interface we configured above
interface=wlan0

# Use the nl80211 driver with the brcmfmac driver
driver=nl80211

# This is the name of the network
ssid=RGS-IOT

# Use the 2.4GHz band
hw_mode=g

# Use channel 1
channel=1

# Enable 802.11n
ieee80211n=1

# Enable WMM
wmm_enabled=1

# Enable 40MHz channels with 20ns guard interval
# ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]

# Accept all MAC addresses
macaddr_acl=0

# Use WPA authentication
auth_algs=1

# Require clients to know the network name
ignore_broadcast_ssid=0

# Use WPA2
wpa=2

# Use a pre-shared key
wpa_key_mgmt=WPA-PSK

# The network passphrase
wpa_passphrase=0shPUhQG

# Use AES, instead of TKIP
rsn_pairwise=CCMP
EOF

sed -i -- '/#DAEMON_CONF=""/c\DAEMON_CONF="/etc/hostapd/hostapd.conf"' /etc/default/hostapd

cat > /etc/dnsmasq.conf <<EOF
interface=wlan0                 # Use interface wlan0
listen-address=172.24.1.1       # Explicitly specify the address to listen on
bind-interfaces                 # Bind to the interface to make sure we aren't sending things elsewhere
server=8.8.8.8                  # Forward DNS requests to Google DNS
domain-needed                   # Don't forward short names
bogus-priv                      # Never forward addresses in the non-routed address spaces.
dhcp-range=172.24.1.50,172.24.1.150,12h # Assign IP addresses between 172.24.1.50 and 172.24.1.150 with a 12 hour lease time
EOF

echo "Enabling packet forwarding..."
sed -i -- 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
sh -c "iptables-save > /etc/iptables.ipv4.nat"

echo "All done! Please reboot"
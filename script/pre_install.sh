#!/usr/bin/env bash

echo "Installing dependencies and some python packages"

sudo apt-get install -y libpython-dev

echo "Installing BME280 library"

cd ~
git clone https://github.com/adafruit/Adafruit_Python_GPIO.git
cd Adafruit_Python_GPIO
sudo python setup.py install
cd ~
sudo rm -r Adafruit_Python_GPIO

git clone https://github.com/adafruit/Adafruit_Python_BME280
cd Adafruit_Python_BME280
sudo python setup.py install
cd ~
sudo rm -r Adafruit_Python_BME280

echo "Installing TSL2591 library"

cd ~
sudo apt-get install -y libffi-dev
git clone https://github.com/maxlklaxl/python-tsl2591.git
cd python-tsl2591
sudo python setup.py install
cd ~
sudo rm -r python-tsl2591

echo "Installing pyaudio library"
sudo apt-get install -y python-pyaudio portaudio19-dev
sudo pip2 install pyaudio
sudo pip2 install numpy
git clone https://github.com/ExCiteS/SLMPi.git
cd SLMPi/SoundAnalyse-0.1.1
sudo python setup.py install
cd ~
sudo rm -r SLMPi

sudo apt-get install -y libmysqlclient-dev
sudo pip2 install MySQL-python

sudo pip2 install peewee adafruit-ads1x15

echo "Finished installation. You can now start reading sensor data!"

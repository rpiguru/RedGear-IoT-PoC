# RedGear IoT PoC

## 1. Device list

- Raspberry Pi Zero W (Node Device)

    https://www.raspberrypi.org/products/raspberry-pi-zero-w/

- Raspberry Pi 3 (Gateway)

    https://www.raspberrypi.org/products/raspberry-pi-3-model-b/

- TSL2591 Luminosity Sensor Module

    https://www.adafruit.com/product/1980

    https://www.amazon.co.uk/Adafruit-TSL2591-Dynamic-Digital-Sensor/dp/B00M40Y0NW

- Ultrasonic Sensor Module (HC-SR04)

    https://www.amazon.co.uk/HC-SR04-Ultrasonic-Distance-Rangefinder-Detection/dp/B0066X9V5K

- PIR Sensor Module (HC-SR501)

    https://www.amazon.co.uk/HiLetgo-HC-SR501-Pyroelectric-Infrared-Product/dp/B007XQRKD4

- Temperature, Humidity, Pressure Sensor (BME280)

    https://www.amazon.co.uk/MagiDeal-Temperature-Humidity-Barometric-Pressure/dp/B01GQ3T1A4

- USB Microphone

    https://www.amazon.co.uk/Richera-Microphone-Notebook-Recognition-Software/dp/B01FJWO5K4/ref=pd_cp_107_3?_encoding=UTF8&psc=1&refRID=0F4J0PPSJHKGHPY9M527

    Mini OTG Converter

    https://www.amazon.co.uk/Joyshare-Micro-USB-OTG-Adapter/dp/B01DOEY522/ref=sr_1_7?ie=UTF8&qid=1507200761&sr=8-7&keywords=mini+otg+usb

- Push Button, LED

    https://www.amazon.co.uk/Sintron-Extension-Starter-Breadboard-Raspberry-Pi-LED10-B/dp/B00WSXXLZS/ref=br_lf_m_ywsqpnphrc4w8ot_ttl?_encoding=UTF8&s=electronics



## 2. Connecting Sensors

![Schematic View](img/schematic_schem.jpg)

![Breadboard View](img/schematic_bb.jpg)


  *NOTE*: All Pin numbers are defined in `config.ini`

- RPi & TSL2591 Luminosity Sensor

    | **RPi** |  ** TSL2591** |
    | :----:  |    :----:     |
    |  3.3V   |      3v3      |
    |   GND   |      GND      |
    |   SDA   |      SDA      |
    |   SCL   |      SCL      |

- RPi & BME280

    | **RPi** |   **BME280**  |
    | :----:  |    :----:     |
    |  3.3V   |      3v3      |
    |   GND   |      GND      |
    |   SDA   |      SDA      |
    |   SCL   |      SCL      |

- RPi & PIR Sensor

    | **RPi** |    **PIR**    |
    | :----:  |    :----:     |
    |    5V   |      VCC      |
    |   GND   |      GND      |
    |  GPIO4  |      SIG      |

- RPi & Ultrasonic Sensor

    | **RPi** |**Ultrasonic** |
    | :----:  |    :----:     |
    |    5V   |      VCC      |
    |   GND   |      GND      |
    | GPIO24  |      ECHO     |
    | GPIO23  |      TRIG     |

- LED: `GPIO21`

- Push Button: `GPIO20`


## 3. Installation

1) Install Raspbian on RPi.

TBD


- Setup WiFi AP on RPi 3 (Gateway)

        sudo bash script/rpi3_ap_setup.sh


2) Enable I2C on RPi.

  https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c

3) Install packages

        bash script/pre_install.sh
